﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using NullFX.CRC;

[assembly: InternalsVisibleTo("InteractiveReaderLibrary.Tests")]

namespace InteractiveReaderLibrary
{

    public enum Module : byte
    {
        ContactCardsModule = 0x1,
        ContactLessLevelOneModule = 0x2,
        MifareCardsModule = 0x3,
        ServiceOperationsModule = 0x4,
        NxpNtagCardsModule = 0x5,
        GuiModule = 0x6,
    }

    internal enum MessageType : byte
    {
        CommandMessage = 0x1,
        SuccessfulResponseMessage = 0x2, //final
        FailureResponseMessage = 0x3, //final
        PendingResponseMessage = 0x4 //intermediate
    }


    internal static class MessageSerializer
    {
        private static byte[] ToBigEndian(UInt16 val)
        {
            return BitConverter.IsLittleEndian
                ? BitConverter.GetBytes(val).Reverse().ToArray()
                : BitConverter.GetBytes(val);
        }

        private static UInt16 FromBigEndian(byte[] val)
        {
            return BitConverter.IsLittleEndian
                ? BitConverter.ToUInt16(val.Reverse().ToArray())
                : BitConverter.ToUInt16(val);
        }

        private static void CheckMessageSize(long streamLength)
        {
            if (streamLength < Message.MinSize || streamLength> Message.MaxSize)
            {
                throw new MessageException("Invalid message size");
            }
        }

        public static MemoryStream Serialize(Message message)
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(Message.Header);
            writer.Write(ToBigEndian(message.Length));
            writer.Write(message.MsgId);
            writer.Write((byte)message.Module);
            writer.Write((byte)message.MessageType);
            writer.Write(message.Payload);
            // write CRC
            writer.Write(ToBigEndian(Message.GetCRC(stream.GetBuffer(), (int)stream.Length)));
            CheckMessageSize(stream.Length);
            return stream;
        }

        public static Message UnSerialize(MemoryStream stream)
        {
            CheckMessageSize(stream.Length);

            var buffer = new byte[2];
            // check crc
            var bodyCrc = Message.GetCRC(stream.ToArray(), (int)(stream.Length - 2));
            stream.Seek(stream.Length - 2, SeekOrigin.Begin);
            if (stream.Read(buffer, 0, 2) != 2)
                throw new MessageException("Wrong message");
            var crc = FromBigEndian(buffer);
            if (crc != bodyCrc)
            {
                throw new MessageException("CRC error");
            }

            stream.Position = 0;
            BinaryReader reader = new BinaryReader(stream);

            //read header
            buffer = reader.ReadBytes(2);
            if (!buffer.SequenceEqual(Message.Header))
            {
                throw new MessageException("invalid header");
            }

            // read length
            buffer = reader.ReadBytes(2);

            int length = FromBigEndian(buffer);

            Message message = new Message()
            {
                // read msgId
                MsgId = reader.ReadByte(),
                //read Module
                Module = (Module)reader.ReadByte(),
                //read Type
                MessageType = (MessageType)reader.ReadByte(),
                // read Payload
                Payload = reader.ReadBytes(length - 3)
            };
            return message;
        }
    }

    internal class Message
    {
        internal const UInt16 MinSize = 9;
        internal const UInt16 MaxSize = 4230;

        public Message(Module module, byte[] payload, byte msgId, MessageType messageType) : this()
        {
            MsgId = msgId;
            Module = module;
            Payload = payload;
            MessageType = messageType;
        }

        public Message(Module module, string? payload, byte msgId, MessageType messageType) :
            this(module, Encoding.UTF8.GetBytes(payload ?? string.Empty), msgId, messageType)
        {
        }

        public Message()
        {
            Payload = new byte[] { };
        }

        private ushort GetLength()
        {
            return (ushort)(3 + Payload.GetLength(0));
        }

        internal static ushort GetCRC(byte[] data, int length)
        {
            return Crc16.ComputeChecksum(Crc16Algorithm.Ccitt, data, 0, length);
        }

        public static byte[] Header { get; } = { 0x41, 0x43 };
        public byte MsgId { get; set; }

        public ushort Length => this.GetLength();
        public Module Module { get; set; }
        public MessageType MessageType { get; set; }
        public byte[] Payload { get; set; }

        public string GetPayloadStr() => Encoding.UTF8.GetString(Payload);
    }
}