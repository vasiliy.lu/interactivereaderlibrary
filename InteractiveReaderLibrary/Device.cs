using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace InteractiveReaderLibrary
{
    internal readonly struct MessageId : IEquatable<MessageId>
    {
        private readonly Module Module { get; }
        private readonly byte Id { get; }

        public MessageId(Module module, byte id)
        {
            Module = module;
            Id = id;
        }
        public bool Equals(MessageId other) => Id==other.Id && Module==other.Module;
        public override bool Equals(object? obj) => Equals((MessageId)obj!);
        private ushort GetKey() => (ushort)(((byte)Module << 8) + Id);
        public override int GetHashCode() => GetKey().GetHashCode();
    }

    internal class RequestDataItem
    {
        public AutoResetEvent RequestEvent { get; }
        public Message? ResponseMessage { get; set; }
        
        public RequestDataItem(AutoResetEvent requestEvent)
        {
            RequestEvent = requestEvent;
        }
    } 
    
    public sealed class InteractiveReaderDevice : IDisposable
    {
        private readonly Dictionary<Module, byte> _msgIdGenerator = new Dictionary<Module, byte>();

        private readonly ConcurrentDictionary<MessageId, RequestDataItem > _requestEvents =
            new ConcurrentDictionary<MessageId, RequestDataItem>();

        private IPhysicalPort? _physicalPort;
        private readonly AutoResetEvent _portEvent = new AutoResetEvent(false);

        private readonly object _deviceObj = new object();
        private readonly object _idGeneratorObj = new object();
        private bool _continue = true;
        private readonly Thread _readThread;

        public IPhysicalPort? Port
        {
            set
            {
                lock (_deviceObj)
                {
                    _physicalPort = value;
                    _portEvent.Set();
                }
            }
            get => _physicalPort;
        }

        public int RequestTimeout { get; set; } = 5000;

        public delegate void ErrorHandler(object sender, Exception exception);

        public event ErrorHandler? OnError;

        public InteractiveReaderDevice()
        {
            foreach (var mod in (Module[]) Enum.GetValues(typeof(Module)))
            {
                _msgIdGenerator.Add(mod, 0);
            }
            
            _readThread = new Thread(ReadMessagesFromPort);
            _readThread.Start();
        }

        ~InteractiveReaderDevice()
        {
            Dispose(false);
        }

        private static bool IsModuleSupported(Module module)
        {
            return ((IList)new[]
            {
                Module.GuiModule, Module.ContactCardsModule, Module.MifareCardsModule, Module.ServiceOperationsModule,
                Module.NxpNtagCardsModule, Module.ContactLessLevelOneModule
            }).Contains(module);
        }

        private void ProcessThreadException(Exception e)
        {
            OnError?.Invoke(this, e);
        }

        private void ReadMessagesFromPort()
        {
            while (_continue)
                try
                {
                    if (Port is null)
                    {
                        //throw new DeviceException("Port is not assigned");
                        _portEvent.WaitOne();
                        continue;
                    }

                    var binaryMessage = Port.ReadMessage();

                    var responseMessage = MessageSerializer.UnSerialize(binaryMessage);
                    var messageId = new MessageId(responseMessage.Module, responseMessage.MsgId);
                    if (!_requestEvents.TryGetValue(messageId, out var requestData))
                        throw new DeviceException(
                            $"Orphan response. Request event not found for message {responseMessage.MsgId}");
                    requestData.ResponseMessage = responseMessage;

                    requestData.RequestEvent.Set();
                }
                catch (TimeoutException)
                {
                }
                catch (Exception e)
                {
                    ProcessThreadException(e);
                }
        }

        private byte GenMsgId(Module module)
        {
            lock (_idGeneratorObj)
            {
                var msgId = _msgIdGenerator[module]++;
                return msgId;
            }
        }

        private (Message, WaitHandle) CreateRequestMessage(Module module, string payload)
        {
            var waitEvent = new AutoResetEvent(false);
            byte msgId;
            int safeCounter = 0;
            do
            {
                msgId = GenMsgId(module);
                if (safeCounter++ > 255)
                    throw new DeviceException($"Too much requests to module {module}");
            } while (!_requestEvents.TryAdd(new MessageId(module, msgId), new RequestDataItem(waitEvent)));

            var reqMessage = new Message(module, payload, msgId, MessageType.CommandMessage);
            return (reqMessage, waitEvent);
        }

        private Message WaitEvent(MessageId msgId, WaitHandle reqEvent, int timeout)
        {
            if (reqEvent.WaitOne(timeout))
            {
                var requestDataItem = _requestEvents[msgId];
                if (requestDataItem.ResponseMessage == null) throw new DeviceException("Response message is null");

                return requestDataItem.ResponseMessage;
            }

            throw new TimeoutException($"Request timeout ({RequestTimeout})");
        }

        private (MessageId, WaitHandle) WriteMessage(Module module, string payload)
        {
            if (Port is null) throw new InvalidDataException("Port is not assigned");

            var (reqMessage, waitEvent) = CreateRequestMessage(module, payload);
            
            Port.WriteMessage(MessageSerializer.Serialize(reqMessage));
            return (new MessageId(reqMessage.Module, reqMessage.MsgId), waitEvent);
        }

        public Response SendMessage(Module module, string payload, int? timeout = null)
        {
            if (!IsModuleSupported(module))
                throw new NotSupportedException($"Module {module} not supports");

            var (msgId, waitEvent) = WriteMessage(module, payload);
            try
            {
                Message responseMessage;
                do
                {
                    responseMessage = WaitEvent(msgId, waitEvent, timeout ?? RequestTimeout);
                } while (responseMessage.MessageType == MessageType.PendingResponseMessage);

                var response = new Response(responseMessage.GetPayloadStr(), responseMessage.MessageType switch
                {
                    MessageType.SuccessfulResponseMessage => Status.Successful,
                    MessageType.FailureResponseMessage => Status.Failed,
                    _ => throw new ArgumentException($"Invalid response status {responseMessage.MessageType}")
                });
                return response;
            }
            finally
            {
                _requestEvents.Remove(msgId,out var _);
            }
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            _continue = false;
            Port = null;
            _readThread.Join();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}