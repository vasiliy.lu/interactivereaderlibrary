using System.IO;

namespace InteractiveReaderLibrary
{

    public interface IPhysicalPort
    {
        public void WriteMessage(MemoryStream binaryMessage);

        public MemoryStream ReadMessage();
    }
}