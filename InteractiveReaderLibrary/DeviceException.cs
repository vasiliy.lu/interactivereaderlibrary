using System;
using System.Runtime.Serialization;

namespace InteractiveReaderLibrary
{

    [Serializable()]
    public class DeviceException : Exception
    {
        public DeviceException(string message) : base(message)
        {
        }

        protected DeviceException(SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}