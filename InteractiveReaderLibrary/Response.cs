namespace InteractiveReaderLibrary
{

    public enum Status
    {
        Successful,
        Failed
    }

    public class Response
    {
        public Response(string payload, Status status)
        {
            Payload = payload;
            Status = status;
        }

        public string Payload { get; }
        public Status Status { get; }
    }
}