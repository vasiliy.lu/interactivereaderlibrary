using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InteractiveReaderLibrary.Tests;

internal delegate void OnWork(Message response); 
public class TestSubSystem
{
    private readonly object _obj = new() ;
    public TestSubSystem(Module module, int requestProcessTime)
    {
        Module = module;
        RequestProcessTime = requestProcessTime;
    }

    public Module Module { get; init; }
    public int RequestProcessTime { get; set; }
    public int PendingTime { get; set; } = 0;
    public Status AnswerStatus { get; set; } = Status.Successful;

    internal void ProcessRequest(Message request, string? answerPayload, OnWork onWork)
    {
        lock (_obj)
        {
            var payload = answerPayload != null ? Encoding.UTF8.GetBytes(answerPayload) : request.Payload;
            
            if (PendingTime > 0)
            {
                onWork(new Message(Module,payload,request.MsgId,MessageType.PendingResponseMessage));
                Thread.Sleep(PendingTime);
            }
            Thread.Sleep(RequestProcessTime);
            onWork(new Message(Module, payload, request.MsgId,
                AnswerStatus == Status.Successful
                    ? MessageType.SuccessfulResponseMessage
                    : MessageType.FailureResponseMessage));
        }
    }
}