using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace InteractiveReaderLibrary.Tests;

public class Tests
{
    [Test]
    public void Test_TimeoutThrow()
    {
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReadTimeout = 10000
        };
        port.SubSystems[Module.GuiModule].RequestProcessTime = 500;
        device.Port = port;
        device.RequestTimeout = 300;
        // send request
        Assert.Throws<TimeoutException>(() => device.SendMessage(Module.GuiModule, "test"));
    }

    [TestCase(3, 300)]
    [TestCase(260, 10)]
    public void Test_Multi(int requestsCount, int answerDelay)
    {
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReadTimeout = 1000,
        };
        port.SubSystems[Module.GuiModule].RequestProcessTime = answerDelay;
        device.Port = port;
        device.RequestTimeout = 100+answerDelay * 2;
        // send requests
        for (int i = 0; i < requestsCount; i++)
        {
            string payload = $"Req {i} payload";
            var response = device.SendMessage(Module.GuiModule, payload);
            Assert.AreEqual(payload, response.Payload);
        }

        //Assert.AreEqual(0, device.BackgroundExceptions.Count);
    }

    [Test]
    public void Test_TooMuchRequestOK()
    {
        const int requestsCount = 300;
        const int answerDelay = 30;
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new();
        port.SubSystems[Module.GuiModule].RequestProcessTime = answerDelay;
        device.Port = port;
        device.RequestTimeout = answerDelay*2*requestsCount;
        // send requests
        List<Task> tasks = new();
        for (int i = 0; i < requestsCount; i++)
        {
            var i1 = i;
            tasks.Add(Task.Factory.StartNew(() =>
            {
                var payload = $"Req {i1} payload";
                var response = device.SendMessage(Module.GuiModule, payload);
                Assert.AreEqual(payload, response.Payload);
                Assert.AreEqual(Status.Successful, response.Status);
            },TaskCreationOptions.LongRunning));
            Thread.Sleep(10);
        }
        
        Task.WhenAll(tasks.ToArray()).Wait();
    }

    [Test]
    public void Test_TooMuchRequestFailed()
    {
        const int requestsCount = 1000;
        const int answerDelay = 20;
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new();
        port.SubSystems[Module.GuiModule].RequestProcessTime = answerDelay;
        device.Port = port;
        device.RequestTimeout = answerDelay*2*requestsCount;
        // send requests
        List<Task> tasks = new();
        for (int i = 0; i < requestsCount; i++)
        {
            var i1 = i;
            tasks.Add(Task.Factory.StartNew(() =>
            {
                var payload = $"Req {i1} payload";
                var response = device.SendMessage(Module.GuiModule, payload);
                Assert.AreEqual(payload, response.Payload);
                Assert.AreEqual(Status.Successful, response.Status);
            },TaskCreationOptions.LongRunning));
        }
        
        var ae = Assert.Throws<AggregateException>(() => Task.WhenAll(tasks.ToArray()).Wait());
        Assert.AreEqual($"Too much requests to module {Module.GuiModule}",
            ae?.InnerExceptions.First().Message);
    }

    [TestCase(5, 200)]
    [TestCase(10, 100)]
    public void Test_MultiAsyncOneModule(int requestsCount, int answerDelay)
    {
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new();
        port.SubSystems[Module.GuiModule].RequestProcessTime = answerDelay;
        device.Port = port;
        device.RequestTimeout = 1000 + answerDelay * 2;
        // send requests
        List<Task> tasks = new();
        for (int i = 0; i < requestsCount; i++)
        {
            var i1 = i;
            tasks.Add(Task.Factory.StartNew(() =>
            {
                string payload = $"Req {i1} payload";
                var response = device.SendMessage(Module.GuiModule, payload);
                //Assert.AreEqual(answerDelay,sw.ElapsedMilliseconds,100);
                Assert.AreEqual(payload, response.Payload);
                Assert.AreEqual(Status.Successful, response.Status);
            },TaskCreationOptions.LongRunning));
        }
        
        Task.WhenAll(tasks.ToArray()).Wait();
        
    }

    [Test]
    public void Test_FailedResponse()
    {
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReadTimeout = 200
        };
        port.SubSystems[Module.GuiModule].AnswerStatus = Status.Failed;
        device.Port = port;
        device.RequestTimeout = 1000;
        // send requests
        string payload = $"Req payload";

        var response = device.SendMessage(Module.GuiModule, payload);

        Assert.AreEqual(Status.Failed, response.Status);
    }

    [Test]
    public void Test_PendingResponse()
    {
        // connect to device
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReadTimeout = 200,
        };
        port.SubSystems[Module.GuiModule].RequestProcessTime = 200;
        port.SubSystems[Module.GuiModule].PendingTime = 200;
        device.Port = port;
        device.RequestTimeout = 500;
        // send requests
        string payload = $"Req payload";

        var response = device.SendMessage(Module.GuiModule, payload);

        Assert.AreEqual(payload, response.Payload);
        Assert.AreEqual(Status.Successful, response.Status);
    }

    [Test]
    public void TestSubSystemsWithDifferentTime()
    {
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReadTimeout = 500,
        };
        device.RequestTimeout = 2000;
        device.Port = port;

        List<(Module, int, int)> modulesForTest = new()
        {
            (Module.GuiModule, 100,0),
            (Module.ServiceOperationsModule, 100,1000)
        };

        foreach (var mod in modulesForTest)
        {
            port.SubSystems[mod.Item1].RequestProcessTime = mod.Item2;
            port.SubSystems[mod.Item1].RequestProcessTime = mod.Item3;
        }

        List<Task> tasks = new();
        foreach (var mod in modulesForTest)
        {
            tasks.Add(Task.Factory.StartNew(() =>
            {
                var pld = $"{mod.Item1} task";
                var sw = Stopwatch.StartNew();
                var resp = device.SendMessage(mod.Item1, pld);
                sw.Stop();
                //Assert.AreEqual(mod.Item2+mod.Item3, sw.ElapsedMilliseconds, 100);
                Assert.AreEqual(pld, resp.Payload);
                Assert.AreEqual(Status.Successful, resp.Status);
            }));
        }

        Task.WaitAll(tasks.ToArray());
    }

    [Test]
    public void Test_WrongDataInResponse()
    {
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReturnWrongData = true
        };
        device.RequestTimeout = 500;
        device.Port = port;
        Assert.Throws<TimeoutException>(() => device.SendMessage(Module.GuiModule, ""));
        //Assert.IsTrue(device.BackgroundExceptions.Any((exception => exception is MessageException)));
    }

    [Test]
    public void Test_ExceptionEventHandler()
    {
        using var device = new InteractiveReaderDevice();
        TestEchoPhysicalPort port = new()
        {
            ReturnWrongData = true
        };
        var handledExceptions = new List<string>();
        device.OnError += (_, exception) =>
        {
            handledExceptions.Add(exception.Message);
            device.Port = null;
        }; 
        device.Port = port;
        Thread.Sleep(200);
        device.Dispose();
        Assert.AreEqual(1,handledExceptions.Count);
    }
}