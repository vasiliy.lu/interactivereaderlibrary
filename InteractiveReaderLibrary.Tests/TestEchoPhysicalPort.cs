using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace InteractiveReaderLibrary.Tests;

using InteractiveReaderLibrary;

public class TestEchoPhysicalPort : IPhysicalPort
{
    public IReadOnlyDictionary<Module, TestSubSystem> SubSystems { get; } = new ConcurrentDictionary<Module, TestSubSystem>()
    {
        [Module.GuiModule] = new TestSubSystem(Module.GuiModule, 0),
        [Module.ContactCardsModule] = new TestSubSystem(Module.ContactCardsModule, 0),
        [Module.MifareCardsModule] = new TestSubSystem(Module.MifareCardsModule, 0),
        [Module.ServiceOperationsModule] = new TestSubSystem(Module.ServiceOperationsModule, 0),
        [Module.NxpNtagCardsModule] = new TestSubSystem(Module.NxpNtagCardsModule, 0),
        [Module.ContactLessLevelOneModule] = new TestSubSystem(Module.ContactLessLevelOneModule, 0),
    };
    public string? AnswerPayload { get; set; }
    public bool ReturnWrongData { get; set; }
    public int ReadTimeout { get; set; } = 1000;
    private readonly ConcurrentBag<MemoryStream> _listMessagesForReading = new();
    private readonly object _messagesEvent = new();

    // private void _WriteMessage(MemoryStream binaryMessage)
    // {
    //     var msg = MessageSerializer.UnSerialize(binaryMessage);
    //     Console.WriteLine($"{GetType().Name}: WriteMessage... (id: {msg.MsgId})");
    //     _listMessagesForReading.Add(new MemoryStream(binaryMessage.ToArray()));
    //     lock (_messagesEvent)
    //     {
    //         Monitor.PulseAll(_messagesEvent);
    //     }
    //     Console.WriteLine($"{GetType().Name}: WriteMessage...ok (id: {msg.MsgId})");
    // }

    public void WriteMessage(MemoryStream binaryMessage)
    {
        var reqMessage = MessageSerializer.UnSerialize(binaryMessage);

        Task.Factory.StartNew(() =>
        {
            SubSystems[reqMessage.Module].ProcessRequest(reqMessage, AnswerPayload, message =>
            {
                _listMessagesForReading.Add(MessageSerializer.Serialize(message));
                lock (_messagesEvent)
                {
                    Monitor.PulseAll(_messagesEvent);
                }
            });
            
        });
    }

    public MemoryStream ReadMessage()
    {
        //Console.WriteLine($"{GetType().Name}: Reading message...");
        
        if (ReturnWrongData)
        {
            return new MemoryStream(new byte[] { 1, 2, 3 });
        }

        if (_listMessagesForReading.IsEmpty)
        {
            lock (_messagesEvent)
            {
                if (!Monitor.Wait(_messagesEvent, ReadTimeout))
                    throw new TimeoutException("Port timeout");
            }
        }

        if (!_listMessagesForReading.TryTake(out var responseMessageStream))
            throw new DataException("List is empty :(");

        return responseMessageStream;
    }
}